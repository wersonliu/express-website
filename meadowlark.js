const express = require('express');
const fortune=require('./lib/fortune');
var handlebars = require('express3-handlebars')
    .create({ defaultLayout:'main' });

const app = express();

//设置模板引擎
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
//设置静态文件
app.use(express.static(__dirname + '/public'));

app.set('port', 3000);

app.get('/', function (req, res) {
    // res.type('text/plain');
    // res.send('Meadowlark Travel');
    res.render('home')
});


app.get('/about', function (req, res) {
    // res.type('text/plain');
    // res.send('About Meadowlark Travel');
    res.render('about',{fortune:fortune.getFortune()})
});


app.use(function (req, res) {
    res.status(404);
    res.render('404');
});


app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

app.get('/headers', function(req,res){
    res.set('Content-Type','text/plain');
    var s = '';
    for(var name in req.headers) s += name + ': ' + req.headers[name] + '\n';
    res.send(s);
});
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.');
});